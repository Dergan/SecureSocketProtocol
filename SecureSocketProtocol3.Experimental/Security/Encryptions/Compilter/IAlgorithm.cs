﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureSocketProtocol3.Experimental.Security.Encryptions.Compilter
{
    public interface IAlgorithm
    {
        ulong CalculateULong(ulong Value);
        //byte CalculateByte(byte Value);
    }
}
