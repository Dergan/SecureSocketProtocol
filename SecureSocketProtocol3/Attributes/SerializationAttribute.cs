﻿using SecureSocketProtocol3.Security.Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SecureSocketProtocol3.Attributes
{
    public class SerializationAttribute : Attribute
    {
        public delegate void Invoky();
        public ISerialization Serializer { get; private set; }
        public SerializationAttribute(Type SerializationType)
        {
            this.Serializer = Activator.CreateInstance(SerializationType) as ISerialization;

            if (Serializer == null)
            {
                throw new Exception("Type must be inherited by ISerialization");
            }
        }

        public SerializationAttribute(ISerialization Serializer)
        {
            if (Serializer == null)
                throw new ArgumentNullException("Serializer");

            this.Serializer = Serializer;
        }
    }
}