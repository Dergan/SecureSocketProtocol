﻿using SecureSocketProtocol3.Security.DataIntegrity;
using SecureSocketProtocol3.Security.NetProviders;
using SecureSocketProtocol3.Security.Serialization;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace SecureSocketProtocol3
{
    public abstract class ClientProperties
    {
        public abstract string HostIp { get; }
        public abstract ushort Port { get; }
        public abstract int ConnectionTimeout { get; }

        public abstract byte[] NetworkKey { get; }

        /// <summary> If keyfiles are being used it will make it harder to decrypt the traffic </summary>
        public abstract Stream[] KeyFiles { get; }

        /// <summary>
        /// The Default Serializer will only be used if a Message you're going to send did not specified the serializer
        /// </summary>
        public abstract ISerialization DefaultSerializer { get; }

        public abstract NetProvider NetProvider { get; }

        public ClientProperties()
        {

        }
    }
}