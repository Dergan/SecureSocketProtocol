﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecureSocketProtocol3.Collections
{
    internal class ClientCollection
    {
        private List<SSPClient> _clients;

        public ClientCollection()
        {
            _clients = new List<SSPClient>();
        }

        public int Count
        {
            get
            {
                return _clients.Count;
            }
        }
        

        public void Add(SSPClient item)
        {
            lock (_clients)
            {
                _clients.Add(item);
            }
        }

        public bool Contains(SSPClient item)
        {
            lock(_clients)
            {
                return _clients.Any(o => o.ClientId == item.ClientId);
            }
        }

        public bool Remove(SSPClient item)
        {
            lock (_clients)
            {
                return _clients.Any(o => o.ClientId == item.ClientId);
            }
        }

        public SSPClient Get(SSPClient item)
        {
            lock (_clients)
            {
                return _clients.FirstOrDefault(o => o.ClientId == item.ClientId);
            }
        }

        public SSPClient Get(Guid ClientId)
        {
            lock (_clients)
            {
                return _clients.FirstOrDefault(o => o.ClientId == ClientId);
            }
        }
    }
}