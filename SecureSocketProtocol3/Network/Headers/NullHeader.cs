﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecureSocketProtocol3.Network.Headers
{
    [ProtoContract]
    public sealed class NullHeader : Header
    {
        public NullHeader()
            : base()
        {

        }

        public override Version Version
        {
            get { return new Version(0, 0, 0, 1); }
        }

        public override string HeaderName
        {
            get { return "Null Header"; }
        }
    }
}