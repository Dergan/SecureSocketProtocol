﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecureSocketProtocol3.Network.Headers
{
    /// <summary>
    /// This header is used for the actual socket connection
    /// Using a serializer for the actual header which starts at every packet/message
    /// Will make it less prone to cause connection errors because we added a new variable
    /// </summary>
    [ProtoContract]
    internal sealed class SocketHeader : Header
    {
        [ProtoMember(1)]
        public byte CurPacketId;

        [ProtoMember(2)]
        public ushort ConnectionId;

        [ProtoMember(3)]
        public ushort HeaderId;

        [ProtoMember(4)]
        public uint MessageId;

        [ProtoMember(5)]
        public byte[] SerializedHeader;

        public override string HeaderName
        {
            get
            {
                return "Socket Header";
            }
        }

        public override Version Version
        {
            get
            {
                return new Version(0, 1, 0);
            }
        }
    }
}
