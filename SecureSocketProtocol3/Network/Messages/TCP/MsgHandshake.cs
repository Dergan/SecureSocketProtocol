﻿using ProtoBuf;
using SecureSocketProtocol3.Security.Serialization;

namespace SecureSocketProtocol3.Network.Messages.TCP
{
    [ProtoContract]
    [Attributes.Serialization(typeof(ProtobufSerialization))]
    public class MsgHandshake : IMessage
    {
        [ProtoMember(1)]
        public byte[] Data { get; set; }

        public MsgHandshake(byte[] Data)
            : base()
        {
            this.Data = Data;
        }
        public MsgHandshake()
            : base()
        {

        }

        public override void ProcessPayload(SSPClient client, OperationalSocket OpSocket)
        {
            SSPClient _client = client as SSPClient;
            if (_client != null)
            {
                
            }
        }
    }
}