﻿using ProtoBuf;
using SecureSocketProtocol3.Attributes;
using SecureSocketProtocol3.Security.Serialization;
using SecureSocketProtocol3.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecureSocketProtocol3.Network.Messages.TCP
{
    [ProtoContract]
    [Attributes.Serialization(typeof(ProtobufSerialization))]
    internal class MsgKeepAlive : IMessage
    {
        [ProtoMember(1)]
        public byte[] Payload { get; set; }

        public MsgKeepAlive()
            : base()
        {
            SecureRandom rnd = new SecureRandom();
            this.Payload = rnd.NextBytes(rnd.Next(16, 32));

            for (int i = 0; i < Payload.Length; i++)
                Payload[i] = 0xFF;
        }

        public override void ProcessPayload(SSPClient client, OperationalSocket OpSocket)
        {

        }
    }
}