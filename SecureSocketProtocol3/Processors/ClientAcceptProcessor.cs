﻿using SecureSocketProtocol3.Security.Handshakes;
using SecureSocketProtocol3.Security.NetProviders;
using SecureSocketProtocol3.Utils;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace SecureSocketProtocol3.Processors
{
    internal class ClientAcceptProcessor
    {
        public ClientAcceptProcessor()
        {

        }

        public void ProcessClient(SSPServer Server, TcpListener TcpServer, IAsyncResult result)
        {
            try
            {
                TcpClient AcceptSocket = TcpServer.EndAcceptTcpClient(result); //<- can throw a error
                SSPClient client = Server.GetNewClient(AcceptSocket);
                client.netProvider.server_onConnect();

                if (AcceptSocket.Client.AddressFamily == AddressFamily.InterNetworkV6)
                    client.RemoteIp = ((IPEndPoint)AcceptSocket.Client.RemoteEndPoint).Address.ToString();
                else
                    client.RemoteIp = AcceptSocket.Client.RemoteEndPoint.ToString().Split(':')[0];

                client.Server = Server;

                Server.PreComputes.SetPreNetworkKey(client);
                Server.PreComputes.ComputeNetworkKey(client);

                client.Connection = new Network.Connection(client);
                client.ClientId = Guid.NewGuid();

                SysLogger.Log($"Accepted peer {client.RemoteIp}", SysLogType.Debug);

                lock (Server.Clients)
                {
                    Server.Clients.Add(client.ClientId, client);
                }

                client.onApplyLayers(client.layerSystem);
                client.onApplyHandshakes(client.handshakeSystem);
                client.handshakeSystem.RegisterMessages(client.MessageHandler);

                try
                {
                    client.onBeforeConnect();
                }
                catch (Exception ex)
                {
                    SysLogger.Log(ex.Message, SysLogType.Error, ex);
                    client.onException(ex, ErrorType.UserLand);
                }
                
                client.Connection.StartReceiver();

                //there are no handshakes
                if (client.handshakeSystem.CompletedAllHandshakes)
                {
                    client.Connection.CreateNewThread(new System.Threading.ThreadStart(() =>
                    {
                        try
                        {
                            client.onConnect();
                        }
                        catch (Exception exx)
                        {
                            SysLogger.Log(exx.Message, SysLogType.Error, exx);
                        }
                    })).Start();
                }

                if (!client.handshakeSystem.CompletedAllHandshakes)
                {
                    Handshake curHandshake = client.handshakeSystem.GetCurrentHandshake();
                    curHandshake.CallStartHandshake();
                    curHandshake.FinishedInitialization = true;
                }
            }
            catch (Exception ex)
            {
                SysLogger.Log(ex.Message, SysLogType.Error, ex);
            }
        }
    }
}