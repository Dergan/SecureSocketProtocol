﻿using SecureSocketProtocol3.Network;
using SecureSocketProtocol3.Processors;
using SecureSocketProtocol3.Utils;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace SecureSocketProtocol3
{
    public abstract class SSPServer : IDisposable
    {
        /// <summary> Get a new initialized client that can be used for the server </summary>
        public abstract SSPClient GetNewClient(TcpClient AcceptedClient);

        internal object AuthLock = new object();
        internal TcpListener TcpServer { get; private set; }
        internal TcpListener TcpServer6 { get; private set; }
        public ServerProperties serverProperties { get; private set; }
        internal SortedList<Guid, SSPClient> Clients { get; private set; }

        private ClientAcceptProcessor ClientAcceptProcessor4; //IPv4
        private ClientAcceptProcessor ClientAcceptProcessor6; //IPv6

        internal ClientPrecomputes PreComputes { get; private set; }

        /// <summary>
        /// Initialize a new SSPServer
        /// </summary>
        /// <param name="serverProperties">The properties for the server</param>
        public SSPServer(ServerProperties serverProperties)
        {
            if (serverProperties == null)
                throw new ArgumentNullException("serverProperties");

            this.serverProperties = serverProperties;
            this.Clients = new SortedList<Guid, SSPClient>();

            SysLogger.Log("Starting server", SysLogType.Debug);

            this.ClientAcceptProcessor4 = new ClientAcceptProcessor();
            this.ClientAcceptProcessor6 = new ClientAcceptProcessor();

            this.PreComputes = new ClientPrecomputes();

            //start the server for IPv4
            this.TcpServer = new TcpListener(IPAddress.Parse(serverProperties.ListenIp), serverProperties.ListenPort);
            this.TcpServer.Start();
            this.TcpServer.BeginAcceptTcpClient(AcceptClientCallback, null);

            if (serverProperties.UseIPv4AndIPv6)
            {
                //start the server for IPv6
                this.TcpServer6 = new TcpListener(IPAddress.Parse(serverProperties.ListenIp6), serverProperties.ListenPort);
                this.TcpServer6.Start();
                this.TcpServer6.BeginAcceptTcpClient(AcceptClient6Callback, null);
            }

            SysLogger.Log("Started server", SysLogType.Debug);
        }

        private void AcceptClientCallback(IAsyncResult result)
        {
            try
            {
                this.TcpServer.BeginAcceptTcpClient(AcceptClientCallback, null);
            }
            catch { }

            ClientAcceptProcessor4.ProcessClient(this, TcpServer, result);
        }

        private void AcceptClient6Callback(IAsyncResult result)
        {
            try
            {
                this.TcpServer6.BeginAcceptTcpClient(AcceptClient6Callback, null);
            }
            catch { }

            ClientAcceptProcessor6.ProcessClient(this, TcpServer6, result);
        }

        public SSPClient[] GetClients()
        {
            lock (Clients)
            {
                return Clients.Values.Where(o => o.handshakeSystem.CompletedAllHandshakes).ToArray();
            }
        }

        internal void RemoveClient(SSPClient client)
        {
            if (client != null)
            {
                lock (Clients)
                {
                    if (Clients.ContainsKey(client.ClientId))
                    {
                        Clients.Remove(client.ClientId);
                    }
                }
            }
        }

        public void Dispose()
        {
            TcpServer.Stop();

            if (TcpServer6 != null)
                TcpServer.Stop();

            lock (Clients)
            {
                foreach (SSPClient client in new List<SSPClient>(Clients.Values))
                {
                    client.Disconnect();
                }
            }
        }
    }
}