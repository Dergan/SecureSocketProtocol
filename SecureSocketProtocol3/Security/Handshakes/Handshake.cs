﻿using SecureSocketProtocol3.Network.Headers;
using SecureSocketProtocol3.Network.Messages;
using SecureSocketProtocol3.Network.Messages.TCP;
using SecureSocketProtocol3.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SecureSocketProtocol3.Security.Handshakes
{
    public abstract class Handshake
    {
        public SSPClient Client { get; private set; }

        public abstract void onStartHandshake();
        public abstract void onReceiveMessage(IMessage Message);
        public abstract void onRegisterMessages(MessageHandler messageHandler);
        public abstract void onFinish();
        public abstract MessageProcessMethod ProcessMethod { get; }

        internal bool FinishedInitialization = false;
        internal Stopwatch TimeTaken;

        public bool IsFinished { get; private set; }

        /// <summary>
        /// Enable Layer Protection
        /// </summary>
        public bool EnableLayer { get; private set; }

        public SyncObject HandshakeSync { get; set; }
        public MessageQueue<IMessage> MsgQueue { get; private set; }

        public Handshake(SSPClient Client)
        {
            this.Client = Client;
            this.HandshakeSync = new SyncObject(Client);
            this.HandshakeSync.Value = false;
            this.MsgQueue = new MessageQueue<IMessage>(Client);
        }

        /// <summary>
        /// Send a Handshake message
        /// </summary>
        /// <param name="Message">The message to send</param>
        protected void SendMessage(IMessage Message)
        {
            lock (Client)
            {
                Client.Connection.SendMessage(Message, new SystemHeader());
            }
        }

        /// <summary>
        /// Send a Handshake Message with Header
        /// </summary>
        /// <param name="Message">The message to send</param>
        /// <param name="Header">The Header to send with the message</param>
        protected void SendMessage(IMessage Message, Header Header)
        {
            lock (Client)
            {
                Client.Connection.SendMessage(Message, Header);
            }
        }

        internal void CallStartHandshake()
        {
            this.TimeTaken = Stopwatch.StartNew();
            onStartHandshake();
            FinishedInitialization = true;
        }

        /// <summary>
        /// Let the server/client know we finished the handshake
        /// </summary>
        public void Finish()
        {
            InternalFinish(true);
        }

        internal void InternalFinish(bool SendFinish)
        {
            lock (Client)
            {
                if (!IsFinished)
                {
                    IsFinished = true;

                    if (SendFinish)
                    {
                        Client.Connection.SendMessage(new MsgHandshakeFinish(), new SystemHeader());
                    }

                    HandshakeSync.Value = true;
                    HandshakeSync.Pulse();

                    if(TimeTaken != null)
                        TimeTaken.Stop();

                    onFinish();

                    try
                    {
                        if (!Client.handshakeSystem.CompletedAllHandshakes && Client.IsServerSided)
                        {
                            Handshake curHandshake = Client.handshakeSystem.GetCurrentHandshake();
                            curHandshake.CallStartHandshake();
                            curHandshake.FinishedInitialization = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        SysLogger.Log(ex.Message, SysLogType.Error, ex);
                    }

                    //it looks messy I have to agree
                    try
                    {
                        if (Client.handshakeSystem.CompletedAllHandshakes && Client.IsServerSided)
                        {
                            Client.Connection.CreateNewThread(new System.Threading.ThreadStart(() =>
                            {
                                try
                                {
                                    Client.onConnect();
                                }
                                catch (Exception exx)
                                {
                                    SysLogger.Log(exx.Message, SysLogType.Error, exx);
                                }
                            })).Start();
                        }
                    }
                    catch (Exception ex)
                    {
                        SysLogger.Log(ex.Message, SysLogType.Error, ex);
                    }
                }
            }
        }
    }
}