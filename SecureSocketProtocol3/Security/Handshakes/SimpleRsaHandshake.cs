﻿using ProtoBuf;
using SecureSocketProtocol3.Network.Headers;
using SecureSocketProtocol3.Network.Messages;
using SecureSocketProtocol3.Security.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SecureSocketProtocol3.Security.Handshakes
{
    public class SimpleRsaHandshake : Handshake
    {
        /*
         * 1. Server sends out the Public Key to the client
         * 2. Client will check FingerPrint if it matches the server cert (human-check)
         * 3. Client will send a random 32-byte key to the Server (Encrypted with the Public Key)
         * 4. Client will apply the new key
         * 5. Server receives the message, decrypts it with the Private Key
         * 6. Server applies new key
         */
         
        public delegate bool FingerPrintCheckCallback(byte[] PublicKey, string Md5FingerPrint, string Sha512FingerPrint);
        public event FingerPrintCheckCallback onVerifyFingerPrint;

        private RSACryptoServiceProvider RsaCrypto = new RSACryptoServiceProvider();


        /// <summary>
        /// This constructor is used by the Client
        /// </summary>
        /// <param name="Client"></param>
        public SimpleRsaHandshake(SSPClient Client)
            : base(Client)
        {

        }

        /// <summary>
        /// Initialize the Server handshake
        /// </summary>
        /// <param name="Client"></param>
        /// <param name="PrivateKeyParams">The private key to use</param>
        public SimpleRsaHandshake(SSPClient Client, RSAParameters PrivateKeyParams)
            : base(Client)
        {
            RsaCrypto.ImportParameters(PrivateKeyParams);
        }

        /// <summary>
        /// Initialize the Server handshake
        /// </summary>
        /// <param name="Client"></param>
        /// <param name="PrivateKeyParams">The private key to use in XML format</param>
        public SimpleRsaHandshake(SSPClient Client, string PrivateKeyXML)
            : base(Client)
        {
            RsaCrypto.FromXmlString(PrivateKeyXML);
        }

        public override void onStartHandshake()
        {
            if (base.Client.IsServerSided)
            {
                Task.Run(() => server_HandshakeTask());
            }
            else
            {
                Task.Run(() => client_HandshakeTask());
            }
        }

        private void server_HandshakeTask()
        {
            base.SendMessage(new PublicKeyMessage(RsaCrypto, RsaCrypto.ExportParameters(false)), new NullHeader());

            KeyReplyMessage keyReply = base.MsgQueue.GetTopMessage<KeyReplyMessage>();

            if(keyReply == null)
            {
                //wrong first message ? shouldn't happen...
                base.Client.Disconnect();
                return;
            }

            byte[] NewKey = RsaCrypto.Decrypt(keyReply.NewKey, true);
            base.Client.Connection.ApplyNewKey(NewKey, base.Client.Connection.NetworkKeySalt);
            base.Finish();
        }

        private void client_HandshakeTask()
        {
            PublicKeyMessage publicKey = base.MsgQueue.GetTopMessage<PublicKeyMessage>();

            if(publicKey == null)
            {
                //wrong first message ? shouldn't happen...
                base.Client.Disconnect();
                return;
            }

            RSAParameters PublicParams = new RSAParameters();
            PublicParams.Exponent = publicKey.Exponent;
            PublicParams.Modulus = publicKey.Modulus;
            RsaCrypto.ImportParameters(PublicParams);

            //Verify the data
            if (!RsaCrypto.VerifyData(publicKey.Modulus, new SHA512CryptoServiceProvider(), publicKey.SignedData))
            {
                base.Client.Disconnect();
                return;
            }

            if (onVerifyFingerPrint != null)
            {
                string md5fingerPrint = Convert.ToBase64String(publicKey.Modulus);
                md5fingerPrint = BitConverter.ToString(MD5.Create().ComputeHash(ASCIIEncoding.ASCII.GetBytes(md5fingerPrint)));
                md5fingerPrint = md5fingerPrint.Replace('-', ':');

                string shafingerPrint = Convert.ToBase64String(publicKey.Modulus);
                shafingerPrint = BitConverter.ToString(SHA512.Create().ComputeHash(ASCIIEncoding.ASCII.GetBytes(shafingerPrint)));
                shafingerPrint = shafingerPrint.Replace('-', ':');

                if (!onVerifyFingerPrint(publicKey.Modulus, md5fingerPrint, shafingerPrint))
                {
                    base.Client.Dispose();
                    return;
                }
            }

            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] NewKey = new byte[32];
            rng.GetBytes(NewKey);

            byte[] EncryptedNewKey = RsaCrypto.Encrypt(NewKey, true);

            base.SendMessage(new KeyReplyMessage(EncryptedNewKey), new NullHeader());
            base.Client.Connection.ApplyNewKey(NewKey, base.Client.Connection.NetworkKeySalt);
        }
        
        public override void onReceiveMessage(Network.Messages.IMessage Message)
        {

        }

        public override void onRegisterMessages(MessageHandler messageHandler)
        {
            base.Client.MessageHandler.AddMessage(typeof(PublicKeyMessage), "RSA_PUBLIC_KEY_MESSAGE");
            base.Client.MessageHandler.AddMessage(typeof(KeyReplyMessage), "RSA_REPLY_KEY_MESSAGE");
        }

        public override void onFinish()
        {
            base.Client.MessageHandler.RemoveMessage("RSA_PUBLIC_KEY_MESSAGE");
            base.Client.MessageHandler.RemoveMessage("RSA_REPLY_KEY_MESSAGE");
        }

        public override MessageProcessMethod ProcessMethod
        {
            get
            {
                return MessageProcessMethod.Queue;
            }
        }

        [ProtoContract]
        [Attributes.Serialization(typeof(ProtobufSerialization))]
        public class PublicKeyMessage : IMessage
        {
            [ProtoMember(1)]
            public byte[] Modulus;

            [ProtoMember(2)]
            public byte[] Exponent;

            [ProtoMember(3)]
            public byte[] SignedData;

            public PublicKeyMessage(RSACryptoServiceProvider Crypto, RSAParameters RsaParams)
            {
                this.Modulus = RsaParams.Modulus;
                this.Exponent = RsaParams.Exponent;
                this.SignedData = Crypto.SignData(Modulus, new SHA512CryptoServiceProvider());
            }

            public PublicKeyMessage()
            {

            }

            public override void ProcessPayload(SSPClient client, Network.OperationalSocket OpSocket)
            {

            }
        }


        [ProtoContract]
        [Attributes.Serialization(typeof(ProtobufSerialization))]
        public class KeyReplyMessage : IMessage
        {
            [ProtoMember(1)]
            public byte[] NewKey;

            public KeyReplyMessage(byte[] NewKey)
            {
                this.NewKey = NewKey;
            }

            public KeyReplyMessage()
            {

            }

            public override void ProcessPayload(SSPClient client, Network.OperationalSocket OpSocket)
            {

            }
        }
    }
}