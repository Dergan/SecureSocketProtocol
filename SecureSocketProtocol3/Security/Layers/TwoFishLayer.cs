﻿using SecureSocketProtocol3.Network;
using SecureSocketProtocol3.Security.Encryptions;
using SecureSocketProtocol3.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SecureSocketProtocol3.Security.Layers
{
    public class TwoFishLayer : ILayer
    {
        private TwofishManaged TwoFish;
        private Connection connection;
        private ICryptoTransform encryptTransformer;
        private ICryptoTransform decryptTransformer;

        public TwoFishLayer(Connection connection)
        {
            this.connection = connection;
            this.TwoFish = new TwofishManaged();
        }

        public LayerType Type
        {
            get { return LayerType.Encryption; }
        }

        public void ApplyLayer(byte[] InData, int InOffset, int InLen, ref byte[] OutData, ref int OutOffset, ref int OutLen)
        {
            lock (TwoFish)
            {
                OutOffset = 0;
                OutData = encryptTransformer.TransformFinalBlock(InData, InOffset, InLen);
                OutLen = OutData.Length;
            }
        }

        public void RemoveLayer(byte[] InData, int InOffset, int InLen, ref byte[] OutData, ref int OutOffset, ref int OutLen)
        {
            lock (TwoFish)
            {
                OutOffset = 0;
                OutData = decryptTransformer.TransformFinalBlock(InData, InOffset, InLen);
                OutLen = OutData.Length;
            }
        }

        public void ApplyKey(byte[] Key, byte[] Salt)
        {
            lock (TwoFish)
            {
                this.encryptTransformer = this.TwoFish.CreateEncryptor(KeyExtender(Key, 32), KeyExtender(Salt, 16));
                this.decryptTransformer = this.TwoFish.CreateDecryptor(KeyExtender(Key, 32), KeyExtender(Salt, 16));
            }
        }

        private byte[] KeyExtender(byte[] Input, int TargetLen)
        {
            int temp = 0xFF28423;
            for (int i = 0; i < Input.Length; i++)
                temp += Input[i];

            int oldLen = Input.Length;
            FastRandom rnd = new FastRandom(temp);
            Array.Resize(ref Input, TargetLen);
            rnd.NextBytes(Input, oldLen, TargetLen);
            return Input;
        }
    }
}