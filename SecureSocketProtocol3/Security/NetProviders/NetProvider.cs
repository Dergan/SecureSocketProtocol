﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SecureSocketProtocol3.Security.NetProviders
{
    public abstract class NetProvider
    {
        protected TcpClient Client { get; set; }

        public NetProvider()
        {

        }

        public NetProvider(TcpClient Client)
        {
            this.Client = Client;
        }

        public abstract bool IsConnected { get; }

        //server sided
        public abstract void server_onConnect();

        public abstract Task<bool> ConnectAsync(string Hostname, int Port);
        public abstract bool Connect(string Hostname, int Port);
        public abstract Task WriteAsync(byte[] Data, int Offset, int Length);
        public abstract Task<int> ReadAsync(byte[] Data, int Offset, int Length);


        public abstract void Write(byte[] Data, int Offset, int Length);
        public abstract int Read(byte[] Data, int Offset, int Length);

        public abstract IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState);
        public abstract IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState);

        public abstract int EndRead(IAsyncResult asyncCallback);
        public abstract void EndWrite(IAsyncResult asyncCallback);

        public abstract void Disconnect();
    }
}