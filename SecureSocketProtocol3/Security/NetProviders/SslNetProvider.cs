﻿using Org.BouncyCastle.Crypto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SecureSocketProtocol3.Security.NetProviders
{
    public partial class SslNetProvider : NetProvider
    {
        private SslStream stream;
        private X509Certificate2 Certificate;

        public delegate bool RemoteCertificateCheck(X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors);
        public event RemoteCertificateCheck onRemoteCertificateCheck;

        public override bool IsConnected
        {
            get
            {
                return Client != null && Client.Connected;
            }
        }

        public SslNetProvider()
            : base()
        {

        }

        public SslNetProvider(TcpClient Client, X509Certificate2 Certificate)
            : base(Client)
        {
            this.Certificate = Certificate;
        }

        public override async Task<bool> ConnectAsync(string Hostname, int Port)
        {
            Client = new TcpClient();
            await Client.ConnectAsync(Hostname, Port);
            return Client.Connected;
        }

        public override bool Connect(string Hostname, int Port)
        {
            Client = new TcpClient();
            Client.Connect(Hostname, Port);

            stream = new SslStream(Client.GetStream(), false, CertCheck);
            stream.AuthenticateAsClient(Hostname, null, System.Security.Authentication.SslProtocols.Tls12, true);

            return Client.Connected;
        }

        public override void Disconnect()
        {
            stream?.Close();
            Client?.Close();
            Client?.Dispose();
        }

        private bool CertCheck(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool? IsValidCertificate = onRemoteCertificateCheck?.Invoke(certificate, chain, sslPolicyErrors);
            return IsValidCertificate ?? false;
        }

        public override async Task<int> ReadAsync(byte[] Data, int Offset, int Length)
        {
            return await stream.ReadAsync(Data, Offset, Length);
        }

        public override async Task WriteAsync(byte[] Data, int Offset, int Length)
        {
            await stream.WriteAsync(Data, Offset, Length);
        }

        public override void server_onConnect()
        {
            if(IsConnected)
            {
                stream = new SslStream(Client.GetStream(), false);
                stream.AuthenticateAsServer(Certificate, false, SslProtocols.Tls12, false);
            }
        }

        public override void Write(byte[] Data, int Offset, int Length)
        {
            stream.Write(Data, Offset, Length);
        }

        public override int Read(byte[] Data, int Offset, int Length)
        {
            return stream.Read(Data, Offset, Length);
        }

        public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
        {
            return stream.BeginRead(buffer, offset, count, asyncCallback, asyncState);
        }

        public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
        {
            return stream.BeginWrite(buffer, offset, count, asyncCallback, asyncState);
        }

        public override int EndRead(IAsyncResult asyncCallback)
        {
            return stream.EndRead(asyncCallback);
        }

        public override void EndWrite(IAsyncResult asyncCallback)
        {
            stream.EndWrite(asyncCallback);
        }
    }
}
