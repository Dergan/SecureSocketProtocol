﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SecureSocketProtocol3.Security.NetProviders
{
    public class TcpNetProvider : NetProvider
    {
        private NetworkStream stream;

        public TcpNetProvider()
            : base()
        {

        }

        public TcpNetProvider(TcpClient Client)
            : base(Client)
        {

        }

        public override bool IsConnected
        {
            get
            {
                return Client != null && Client.Connected;
            }
        }

        public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
        {
            return stream.BeginRead(buffer, offset, count, asyncCallback, asyncState);
        }

        public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
        {
            return stream.BeginWrite(buffer, offset, count, asyncCallback, asyncState);
        }

        public override async Task<bool> ConnectAsync(string Hostname, int Port)
        {
            Client = new TcpClient();
            await Client.ConnectAsync(Hostname, Port);
            if (Client.Connected)
            {
                stream = Client.GetStream();
            }
            return Client.Connected;
        }

        public override bool Connect(string Hostname, int Port)
        {
            Client = new TcpClient();

            try
            {
                Client.Connect(Hostname, Port);
            }
            catch(SocketException) { }
            

            if(Client.Connected)
            {
                stream = Client.GetStream();
            }
            
            return Client.Connected;
        }

        public override void Disconnect()
        {
            stream?.Close();
            Client?.Close();
            Client?.Dispose();
        }

        public override void server_onConnect()
        {
            if (IsConnected)
            {
                this.stream = Client.GetStream();
            }
        }

        public override int Read(byte[] Data, int Offset, int Length)
        {
            return stream.Read(Data, Offset, Length);
        }

        public override async Task<int> ReadAsync(byte[] Data, int Offset, int Length)
        {
            return await stream.ReadAsync(Data, Offset, Length);
        }

        public override void Write(byte[] Data, int Offset, int Length)
        {
            stream.Write(Data, Offset, Length);
        }

        public override async Task WriteAsync(byte[] Data, int Offset, int Length)
        {
            await stream.WriteAsync(Data, Offset, Length);
        }

        public override int EndRead(IAsyncResult asyncCallback)
        {
            return stream.EndRead(asyncCallback);
        }

        public override void EndWrite(IAsyncResult asyncCallback)
        {
            stream.EndWrite(asyncCallback);
        }
    }
}
