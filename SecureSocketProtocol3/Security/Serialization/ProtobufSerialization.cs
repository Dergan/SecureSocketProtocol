﻿using ProtoBuf;
using SecureSocketProtocol3.Network.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SecureSocketProtocol3.Security.Serialization
{
    public class ProtobufSerialization : ISerialization
    {
        public byte[] Serialize(Network.Messages.IMessage Message)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Serialize(Message, stream);
                return stream.ToArray();
            }
        }

        public void Serialize(IMessage Message, MemoryStream stream)
        {
            Serializer.Serialize(stream, Message);
        }

        public Network.Messages.IMessage Deserialize(byte[] MessageData, int Offset, int Length, Type MessageType)
        {
            using (MemoryStream ms = new MemoryStream(MessageData, Offset, Length))
            {
                //ms.Position = Offset;
                return (IMessage)Serializer.Deserialize(MessageType, ms);
            }
        }
    }
}
