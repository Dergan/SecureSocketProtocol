﻿using SecureSocketProtocol3.Network.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SecureSocketProtocol3.Utils
{
    public class MessageQueue<MessageType>
    {
        private ConcurrentQueue<MessageType> Queue;
        const int QUEUE_SIZE = 10; //only 10 packets can be in the queue, "max" 10MB memory usage
        private AutoResetEvent resetEvent = new AutoResetEvent(false);
        private SSPClient client;

        public MessageQueue(SSPClient client)
        {
            this.Queue = new ConcurrentQueue<MessageType>();
            this.client = client;
        }

        public Message GetTopMessage<Message>()
        {
            Type TargetType = typeof(Message);
            MessageType message = default(MessageType);

            while (client.Connected)
            {
                if (Queue.TryPeek(out message) && message?.GetType() == TargetType && Queue.TryDequeue(out message))
                {
                    resetEvent.Set();
                    return (Message)Convert.ChangeType(message, TargetType);
                }
                resetEvent.WaitOne(TimeSpan.FromSeconds(1));
            }
            
            return default(Message);
        }

        public void EnqueueMessage(MessageType message)
        {
            while (client.Connected && Queue.Count >= 10)
            {
                //one second time-out, if user is disconnected it will quit
                resetEvent.WaitOne(TimeSpan.FromSeconds(1));
            }

            if (client.Connected)
            {
                Queue.Enqueue(message);
                resetEvent.Set();
            }
        }
    }
}
