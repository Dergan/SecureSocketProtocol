﻿using SecureSocketProtocol3.Network;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SecureSocketProtocol3.Utils
{
    public class TaskQueue<T>
    {
        private Action<T> callback;
        private Queue<T> tasks;
        private Thread taskThread;
        public bool ThreadRunning { get; private set; }
        public uint MaxItems = 100;
        private bool _stop = false;
        private SyncObject syncObj;

        private Connection connection;

        public TaskQueue(Action<T> Callback, Connection connection, uint MaxItems = 100)
        {
            this.tasks = new Queue<T>();
            this.callback = Callback;
            this.MaxItems = MaxItems;
            this.connection = connection;
            this.syncObj = new SyncObject(connection);
            this.taskThread = new Thread(new ThreadStart(WorkerThread));
            this.taskThread.Start();
        }

        public void Enqueue(T value)
        {
            lock (tasks)
            {
                if (connection.Connected && !_stop)
                {
                    tasks.Enqueue(value);

                    syncObj.Value = true;
                    syncObj.Pulse();
                }
            }
        }

        private void WorkerThread()
        {
            ThreadRunning = true;

            while (connection.Connected && !_stop)
            {
                lock (tasks)
                {
                    while (tasks.Count > 0)
                    {
                        try
                        {
                            T obj = tasks.Dequeue();
                            callback(obj);
                        }
                        catch (Exception ex)
                        {
                            SysLogger.Log(ex.Message, SysLogType.Error, ex);
                        }
                    }
                    this.syncObj.Reset();
                }
                syncObj.Wait<bool>(false, 1000);
            }
            ClearTasks();
            ThreadRunning = false;
        }

        public void ClearTasks()
        {
            lock (tasks)
            {
                tasks.Clear();
            }
        }

        public void Stop()
        {
            _stop = true;
        }
    }
}
