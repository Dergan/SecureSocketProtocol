﻿using SecureSocketProtocol3;
using SecureSocketProtocol3.Security.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SecureSocketProtocol3.Security.NetProviders;
using System.Security.Cryptography.X509Certificates;

namespace TestClient
{
    public class ClientProps : ClientProperties
    {

        public override string HostIp
        {
            get
            {
                return "127.0.0.1";
            }
        }

        public override ushort Port
        {
            get { return 5448; }
        }

        public override int ConnectionTimeout
        {
            get { return 30000; }
        }

        public override byte[] NetworkKey
        {
            get
            {
                return new byte[]
                {
                    80, 118, 131, 114, 195, 224, 157, 246, 141, 113,
                    186, 243, 77, 151, 247, 84, 70, 172, 112, 115,
                    112, 110, 91, 212, 159, 147, 180, 188, 143, 251,
                    218, 155
                };
            }
        }

        public override SecureSocketProtocol3.Security.Serialization.ISerialization DefaultSerializer
        {
            get
            {
                return new ProtobufSerialization();
            }
        }

        public override System.IO.Stream[] KeyFiles
        {
            get
            {
                List<MemoryStream> _keyFiles = new List<MemoryStream>();
                _keyFiles.Add(new MemoryStream(new byte[] { 8, 7, 6, 5, 4, 3, 2, 1 }));
                _keyFiles.Add(new MemoryStream(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 }));
                return _keyFiles.ToArray();
            }
        }

        public override NetProvider NetProvider
        {
            get
            {
                SslNetProvider provider = new SslNetProvider();
                provider.onRemoteCertificateCheck += Provider_onRemoteCertificateCheck;
                return provider;
            }
        }

        private bool Provider_onRemoteCertificateCheck(X509Certificate certificate,
                                                       X509Chain chain,
                                                       System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            Console.WriteLine($"Incoming remote certificate Issuer={certificate.Issuer}, Subject={certificate.Subject}");

            X509Certificate2 cert = certificate as X509Certificate2;

            bool IsValidFingerprint = cert.Thumbprint == "C33568D101BC84738CD750631F86A44A92A1A1AE";
            Console.WriteLine($"Fingerprint {cert.Thumbprint} matches server ? {IsValidFingerprint}");

            return IsValidFingerprint;
        }
    }
}