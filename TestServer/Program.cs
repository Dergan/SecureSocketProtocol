﻿using SecureSocketProtocol3;
using SecureSocketProtocol3.Network;
using SecureSocketProtocol3.Security.NetProviders;
using SecureSocketProtocol3.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;

namespace TestServer
{
    class Program
    {
        //public static SortedList<string, User.UserDbInfo> Users;

        static void Main(string[] args)
        {
            //SysLogger.onSysLog += SysLogger_onSysLog;
            Console.Title = "SSP Server";


            //use a real certificate instead of this test

            string CertLocation = "./ServerCert.pfx";
            if(!File.Exists(CertLocation))
            {
                Console.WriteLine("Generating new certificate, cert not found");
                X509Certificate2 cert = SslNetProvider.GenerateCertificate("Test", 2048, DateTime.Now.AddYears(1), DateTime.Now.Subtract(TimeSpan.FromDays(5)));
                byte[] certExported = cert.Export(X509ContentType.Pfx);
                File.WriteAllBytes(CertLocation, certExported);
            }

            X509Certificate2 serverCertificate = new X509Certificate2(CertLocation);

            Server server = new Server(serverCertificate);

            while (true)
            {
                Console.Title = $"SSP Server - Connected Clients: {server.GetClients().Length}";
                Thread.Sleep(250);
            }

            Process.GetCurrentProcess().WaitForExit();
        }

        private static void SysLogger_onSysLog(string Message, SysLogType Type, Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"[SysLogger][{Type}] {Message}");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
